<?php

namespace Drupal\config_export\Plugin\ConfigExport;

use Drupal\config_export\ConfigExportPluginBase;
use Drupal\Core\Serialization\Yaml;

/**
 * Defines git config export plugin.
 *
 * @ConfigExport(
 *   id = "git",
 *   label = @Translation("Git")
 * )
 */
class Git extends ConfigExportPluginBase {

  /**
   * {@inheritdoc}
   */
  public function export() {
    $config_dir = file_directory_temp() . '/config_export';
    rmdir($config_dir);
    mkdir($config_dir);

    // Get raw configuration data without overrides.
    foreach ($this->configManager()->getConfigFactory()->listAll() as $name) {
      $file_name = $config_dir . '/' . $name . '.yml';
      file_put_contents($file_name, Yaml::encode($this->configManager()->getConfigFactory()->get($name)->getRawData()));
    }
    // Get all override data from the remaining collections.
    foreach ($this->targetStorage()->getAllCollectionNames() as $collection) {
      $collection_storage = $this->targetStorage()->createCollection($collection);
      foreach ($collection_storage->listAll() as $name) {
        $dir_name = $config_dir . '/' . str_replace('.', '/', $collection);
        if (!is_dir($dir_name)) {
          mkdir($dir_name);
        }
        file_put_contents($dir_name . "/$name.yml", Yaml::encode($this->configManager()->getConfigFactory()->get($name)->getRawData()));
      }
    }

    // @todo
    // 1. Get get repository details.
    // 2. Delete existing files.
    // 3. add new files.
    // 4. commit and push.
    // @see https://stackoverflow.com/a/11071474/1366019

    // @todo: use github API and update the plugin as github instead of git.
    echo 'pushed to git!';exit;
  }

}
