<?php

namespace Drupal\config_export\Plugin\ConfigExport;

use Drupal\config_export\ConfigExportPluginBase;

/**
 * Defines git config export plugin.
 *
 * @ConfigExport(
 *   id = "s3",
 *   label = @Translation("AWS S3")
 * )
 */
class S3 extends ConfigExportPluginBase {

}
