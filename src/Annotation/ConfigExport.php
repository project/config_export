<?php

namespace Drupal\config_export\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\config_export\ConfigExportDefinition;

/**
 * Defines an Action annotation object.
 *
 * Plugin Namespace: Plugin\ConfigExport.
 *
 * @see \Drupal\Core\Action\ActionInterface
 * @see \Drupal\Core\Action\ActionManager
 * @see \Drupal\Core\Action\ActionBase
 * @see plugin_api
 *
 * @Annotation
 */
class ConfigExport extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the action plugin.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $label;

  /**
   * @return \Drupal\config_export\ConfigExportDefinition
   */
  public function get() {
    return new ConfigExportDefinition($this->definition);
  }

}
