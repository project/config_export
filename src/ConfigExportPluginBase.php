<?php

namespace Drupal\config_export;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Plugin\PluginBase;

/**
 * Provides a default class for Layout plugins.
 */
class ConfigExportPluginBase extends PluginBase implements ConfigExportInterface {

  /**
   * The layout definition.
   *
   * @var \Drupal\Core\Layout\LayoutDefinition
   */
  protected $pluginDefinition;

  /**
   * @var ConfigManagerInterface
   */
  protected $configManager;

  /**
   * @var ConfigManagerInterface
   */
  protected $targetStorage;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->setConfiguration($configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function build(array $regions) {
    // Ensure $build only contains defined regions and in the order defined.
    $build = [];
    foreach ($this->getPluginDefinition()->getRegionNames() as $region_name) {
      if (array_key_exists($region_name, $regions)) {
        $build[$region_name] = $regions[$region_name];
      }
    }
    $build['#settings'] = $this->getConfiguration();
    $build['#layout'] = $this->pluginDefinition;
    $build['#theme'] = $this->pluginDefinition->getThemeHook();
    if ($library = $this->pluginDefinition->getLibrary()) {
      $build['#attached']['library'][] = $library;
    }
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = NestedArray::mergeDeep($this->defaultConfiguration(), $configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function export() {
    return NULL;
  }

  /**
   * Gets config manager.
   *
   * @return \Drupal\Core\Config\ConfigManagerInterface
   */
  protected function configManager() {
    if (!isset($this->configManager)) {
      $this->configManager = \Drupal::getContainer()->get('config.manager');
    }
    return $this->configManager;
  }

  /**
   * Gets storage.
   *
   * @return ConfigManagerInterface
   */
  protected function targetStorage() {
    if (!isset($this->targetStorage)) {
      $this->targetStorage = \Drupal::getContainer()->get('config.storage');
    }
    return $this->targetStorage;
  }

}
